#! /usr/bin/python3
from skpy import Skype
import skypecustom as skypecustom
import base64
import os
import sys
from flask import Flask, request, abort
from dotenv import load_dotenv
load_dotenv()

app = Flask(__name__)

@app.route('/')
def home():
    return "please use /webhook/'<skypeid>'"
@app.route('/webhook/<id>', methods=['POST'])
def webhook(id):
    if request.method == 'POST':
        print(request.json)
        reqjson = request.json
        # id = id
        skypecustom.msgto(id, reqjson)
        return 'success', 200
    else:
        abort(400)
@app.route('/webhook/grafana/<id>', methods=['POST'])
def grafana(id):
    if request.method == 'POST':
        print(request.json)
        reqjson = request.json
        msg = str(reqjson['title']+" "+reqjson['message']+" "+(reqjson['state']).upper())
        skypecustom.msgto(id, msg)
        return 'success', 200
    else:
        abort(400)
if __name__ == '__main__':
    app.run(host="0.0.0.0", port="3080")