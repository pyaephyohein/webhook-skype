import os
from dotenv import load_dotenv
from skpy import Skype
load_dotenv()
username = os.getenv('username')
password = os.getenv('password')
receiver = os.getenv('receiver')
# contact_type = os.getenv('contact_type')

sk = Skype(username, password) # connect to Skype
def msgto(id, msg):
    id = str(id)
    print(id)
    if id.startswith('live'):
        ch = sk.contacts[id].chat
        ch.sendMsg(msg)
        return 'success', 200
    elif id.startswith('19'):
        ch = sk.chats.chat(id)
        ch.sendMsg(msg)
        return 'success', 200
    else:
        print("contact type not supported")
    