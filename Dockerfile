FROM ubuntu:20.04
ENV username='test' password="test"
RUN apt update && apt upgrade -y
RUN apt install -y python3  python3-pip
RUN mkdir -p /home/ubuntu/app
COPY src/. /home/ubuntu/app/
RUN python3 -m pip install -r /home/ubuntu/app/requirements.txt
# VOLUME [ "app" ]
WORKDIR /home/ubuntu/app/
ENTRYPOINT [ "python3"]
CMD [ "main.py" ]